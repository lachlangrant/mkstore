# MKConstants

[![CI Status](http://img.shields.io/travis/lachlangrant/MKConstants.svg?style=flat)](https://travis-ci.org/lachlangrant/MKConstants)
[![Version](https://img.shields.io/cocoapods/v/MKConstants.svg?style=flat)](http://cocoapods.org/pods/MKConstants)
[![License](https://img.shields.io/cocoapods/l/MKConstants.svg?style=flat)](http://cocoapods.org/pods/MKConstants)
[![Platform](https://img.shields.io/cocoapods/p/MKConstants.svg?style=flat)](http://cocoapods.org/pods/MKConstants)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

MKConstants is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'MKConstants'
```

## Author

lachlangrant, lachlangrant@rbvea.co

## License

MKConstants is available under the MIT license. See the LICENSE file for more info.
