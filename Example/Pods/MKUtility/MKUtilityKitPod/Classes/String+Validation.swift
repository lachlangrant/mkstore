//
//  String+Validation.swift
//  MKUtilityKit
//
//  Created by Lachlan Grant on 19/2/17.
//  Copyright © 2017 Lachlan Grant. All rights reserved.
//

import Foundation

public extension String {
    
    public func isEmail() -> Bool {
        return match(Regex.Email.pattern)
    }
    
    public func isNumber() -> Bool {
        return match(Regex.Number.pattern)
    }
    
    public func isUrl() -> Bool {
        return match(Regex.url.pattern)
    }
}
