//
//  MKUtilityKit.h
//  MKUtilityKit
//
//  Created by Lachlan Grant on 19/2/17.
//  Copyright © 2017 Lachlan Grant. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for MKUtilityKit.
FOUNDATION_EXPORT double MKUtilityKitVersionNumber;

//! Project version string for MKUtilityKit.
FOUNDATION_EXPORT const unsigned char MKUtilityKitVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <MKUtilityKit_iOS/PublicHeader.h>


