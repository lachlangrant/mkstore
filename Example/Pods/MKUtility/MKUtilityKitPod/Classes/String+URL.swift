//
//  String+URL.swift
//  MKUtilityKit
//
//  Created by Lachlan Grant on 19/2/17.
//  Copyright © 2017 Lachlan Grant. All rights reserved.
//

import Foundation

public protocol URLStringConvertible {
    var url: URL? { get }
    var string: String { get }
}

extension String: URLStringConvertible {
    
    public var url: URL? {
        return URL(string: self)
    }
    
    public var string: String {
        return self
    }
}
