//
//  TypeAlias.swift
//  MKUtilityKit
//
//  Created by Lachlan Grant on 19/2/17.
//  Copyright © 2017 Lachlan Grant. All rights reserved.
//

public typealias JSONArray = [[String : Any]]
public typealias JSONDictionary = [String : Any]
public typealias Action = () -> Void
