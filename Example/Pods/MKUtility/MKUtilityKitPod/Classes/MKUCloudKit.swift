//
//  MKUCloudKit.swift
//  MKUtilityKit
//
//  Created by Lachlan Grant on 19/2/17.
//  Copyright © 2017 Lachlan Grant. All rights reserved.
//

import Foundation
import CloudKit

public class MKUCloudKit {
    public static let shared = MKUCloudKit()
    public var container: CKContainer = CKContainer.default()
    
    public func saveDataToPrivateDatabase(recordType: String, data: [String: Any], timeout: TimeInterval = 10, callback: @escaping (_ error: Bool, _ message: String) -> Void) {
        let privateDatabase = container.privateCloudDatabase
        
        let record = convertDictionaryToRecord(recordType: recordType, data: data)
        
        let modifyOperation = generateModifyOperation(records: [record], timeout: 10) { (records, recordIDs, error) in
            if error != nil {
                callback(true, (error?.localizedDescription)!)
            } else {
                callback(false, "Save Success")
            }
        }
        
        saveToDatabase(database: privateDatabase, operation: modifyOperation)
    }
    
    public func saveDataToPublicDatabase(recordType: String, data: [String: Any], timeout: TimeInterval = 10, callback: @escaping (_ error: Bool, _ message: String) -> Void) {
        let publicDatabase = container.publicCloudDatabase
        
        let record = convertDictionaryToRecord(recordType: recordType, data: data)
        
        let operation = generateModifyOperation(records: [record], timeout: 10) { (records, recordIDs, error) in
            if error != nil {
                callback(true, (error?.localizedDescription)!)
            } else {
                callback(false, "Save Success")
            }
        }
        
        saveToDatabase(database: publicDatabase, operation: operation)
    }
    
    public func runQueryOnPrivateDatabase(queryStr: String, arguments: [Any], recordType: String, callback: @escaping ([CKRecord]?, Error?) -> Void) {
        let database = container.privateCloudDatabase
        
        let predicate = convertStringToPredicate(str: queryStr, arguments: arguments)
        
        let query = CKQuery(recordType: recordType, predicate: predicate)
        
        runQueryOnDatabase(database: database, query: query, callback: callback)
    }
    
    public func runQueryOnPublicDatabase(queryStr: String, arguments: [Any], recordType: String, callback: @escaping ([CKRecord]?, Error?) -> Void) {
        let database = container.publicCloudDatabase
        
        let predicate = convertStringToPredicate(str: queryStr, arguments: arguments)
        
        let query = CKQuery(recordType: recordType, predicate: predicate)
        
        runQueryOnDatabase(database: database, query: query, callback: callback)
    }
    
    public func updateRecordOnPrivateDatabse(record: CKRecord, data: [String: Any], callback: @escaping (CKRecord?, Error?) -> Void) {
        let newRecord = updateRecord(record: record, data: data)
        
        let privateDB = container.privateCloudDatabase
        
        privateDB.save(newRecord, completionHandler: callback)
    }
    
    public func updateRecordOnPublicDatabase(record: CKRecord, data: [String: Any], callback: @escaping (CKRecord?, Error?) -> Void) {
        let newRecord = updateRecord(record: record, data: data)
        
        let publicDB = container.publicCloudDatabase
        
        publicDB.save(newRecord, completionHandler: callback)
    }
	
	public func setUpSubscriptionInPrivateDatabase(perdicate: NSPredicate, recordType: String, alertLocalizationKey: String, alertSoundName: String, shouldBadge: Bool, callback: @escaping (CKSubscription?, Error?) -> Void) {
		let privateDB = container.privateCloudDatabase
		
		setUpSubscription(database: privateDB, perdicate: perdicate, recordType: recordType, alertLocalizationKey: alertLocalizationKey, alertSoundName: alertSoundName, shouldBadge: shouldBadge, callback: callback)
	}
	
	public func setUpSubscriptionInPublicDatabase(perdicate: NSPredicate, recordType: String, alertLocalizationKey: String, alertSoundName: String, shouldBadge: Bool, callback: @escaping (CKSubscription?, Error?) -> Void) {
		let publicDB = container.publicCloudDatabase
		
		setUpSubscription(database: publicDB, perdicate: perdicate, recordType: recordType, alertLocalizationKey: alertLocalizationKey, alertSoundName: alertSoundName, shouldBadge: shouldBadge, callback: callback)
	}
    
    public func clearPrivateDatabase(recordType: String) {
        let predicate = self.convertStringToPredicate(str: "pro != %@", arguments: [5])
        let query = CKQuery(recordType: recordType, predicate: predicate)
        
        self.runQueryOnDatabase(database: container.privateCloudDatabase, query: query) { (records, error) in
            if ((records?.count)! > 0) {
                for item in records! {
                    self.deletePrivateRecord(item)
                }
            } else {
                MKULog.shared.info("[MKUCloudKit] Nothing to delete")
            }
        }
    }
    
    public func deletePublicRecord(_ record: CKRecord) {
        self.deleteRecord(record, database: container.publicCloudDatabase)
    }
    
    public func deletePrivateRecord(_ record: CKRecord) {
        self.deleteRecord(record, database: container.privateCloudDatabase)
    }
    
    private func deleteRecord(_ record: CKRecord, database: CKDatabase) {
        MKULog.shared.debug("[MKUCloudKit][deleteRecord] Deleting: \(record.recordID)")
        
        let operation = CKModifyRecordsOperation(recordsToSave: nil, recordIDsToDelete: [record.recordID])
        operation.savePolicy = .allKeys
        operation.modifyRecordsCompletionBlock = { added, deleted, error in
            if error != nil {
                MKULog.shared.error("[MKUCloudKit][deleteRecord] Unable to delete record \(record.recordID): \(MKUJSON.toJson(error as Any))")
            } else {
                MKULog.shared.info("[MKUCloudKit][deleteRecord] Deleted record: \(record.recordID)")
            }
        }
        database.add(operation)
    }
	
	private func setUpSubscription(database: CKDatabase, perdicate: NSPredicate, recordType: String, alertLocalizationKey: String, alertSoundName: String, shouldBadge: Bool, callback: @escaping (CKSubscription?, Error?) -> Void) {
		
		let subscription = CKSubscription(recordType: recordType, predicate: perdicate, options: CKSubscriptionOptions.firesOnRecordCreation)
		
		let info = CKNotificationInfo()
		info.alertLocalizationKey = alertLocalizationKey
		info.soundName = alertSoundName
		info.shouldBadge = shouldBadge
		
		subscription.notificationInfo = info
		
		database.save(subscription, completionHandler: callback)
	}

    
    private func updateRecord(record: CKRecord, data: [String: Any]) -> CKRecord{
        for item in data {
            record.setObject(item.value as? CKRecordValue, forKey: item.key)
        }
        
        return record
    }
    
    
    private func convertStringToPredicate(str: String, arguments: [Any]) -> NSPredicate {
        return NSPredicate(format: str, argumentArray: arguments)
    }
    
    private func convertDictionaryToRecord(recordType: String, data: [String: Any]) -> CKRecord {
        let record = CKRecord(recordType: recordType)
        
        for item in data {
            record.setObject(item.value as? CKRecordValue, forKey: item.key)
        }
        
        return record
    }
    
    private func generateModifyOperation(records: [CKRecord], timeout: TimeInterval, callback: @escaping (_ records: [CKRecord]?, _ recordIDs: [CKRecordID]?, _ error: Error?) -> Void) -> CKModifyRecordsOperation {
        let operation = CKModifyRecordsOperation(recordsToSave: records, recordIDsToDelete: nil)
        
//        operation.timeoutIntervalForRequest = timeout
//        operation.timeoutIntervalForResource = timeout
        
        operation.modifyRecordsCompletionBlock = callback
        
        return operation
    }
    
    
    private func saveToDatabase(database: CKDatabase, operation: CKDatabaseOperation) {
        database.add(operation)
    }
    
    private func runQueryOnDatabase(database: CKDatabase, query: CKQuery, callback: @escaping ([CKRecord]?, Error?) -> Void) {
        database.perform(query, inZoneWith: nil, completionHandler: callback)
    }
}
