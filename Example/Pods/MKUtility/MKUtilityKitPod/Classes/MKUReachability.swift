//
//  MKUReachability.swift
//  MKKit
//
//  Created by Lachlan Grant on 19/12/16.
//  Copyright © 2016 Lachlan Grant. All rights reserved.
//

import Foundation
import SystemConfiguration

@available(iOS 9.0, OSX 10.10, *)
public class MKUReachability {
	public init() {
	}

	/// Does the Device have an Internet Connection
	public var isConnectedToNetwork: Bool {
		get {
			var zeroAddress = sockaddr_in()
			zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
			zeroAddress.sin_family = sa_family_t(AF_INET)

			guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
				$0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
					SCNetworkReachabilityCreateWithAddress(nil, $0)
				}
			}) else {
				return false
			}

			var flags: SCNetworkReachabilityFlags = []
			if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
				return false
			}

			let isReachable = flags.contains(.reachable)
			let needsConnection = flags.contains(.connectionRequired)

			return (isReachable && !needsConnection)
		}
	}
}
