//
//  MKUDefaults.swift
//  MKKit
//
//  Created by Lachlan Grant on 19/12/16.
//  Copyright © 2016 Lachlan Grant. All rights reserved.
//

import Foundation

/// MKUDefaults
public class MKUDefaults {

	/// UserDefaults Object
	public var defaults = UserDefaults()
	private var suiteName: String?

	
	/// Init with an AppGroup
	///
	/// - Parameter suiteName: AppGroup Identifier
	public init(suiteName: String) {
		defaults = UserDefaults(suiteName: suiteName)!
		self.suiteName = suiteName
	}

	
	/// Init with Standard Defaults
	public init() {
		defaults = UserDefaults.standard
		suiteName = nil
	}

	/// Remove Suite
	public func remove() {
		guard let suiteName = suiteName else {
			return
		}

		defaults.removeSuite(named: suiteName)
		UserDefaults.standard.removeSuite(named: suiteName)
	}
}
