//
//  UIStoryboard+MKU.swift
//  MKKit
//
//  Created by Lachlan Grant on 19/12/16.
//  Copyright © 2016 Lachlan Grant. All rights reserved.
//

import UIKit

extension UIStoryboard {
    public func getViewController(_ id: String?) -> UIViewController {
        if id == nil {
            return self.instantiateInitialViewController()!
        } else {
            return self.instantiateViewController(withIdentifier: id!)
        }
    }
}

public func getViewController(storyboard: (String, Bundle?), viewController: String?) -> UIViewController {
    var viewInformation: (Bool, String) = (false, "")
    
    if viewController != nil {
        viewInformation.1 = viewController!
    } else {
        viewInformation.0 = true
    }
    
    let bundle = storyboard.1 ?? Bundle.main
    
    let sBoard = UIStoryboard(name: storyboard.0, bundle: bundle)
    
    if viewInformation.0 == true {
        return sBoard.instantiateInitialViewController()!
    } else {
        return sBoard.getViewController(viewInformation.1)
    }
}
