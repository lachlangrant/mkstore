//
//  MKULog.swift
//  MKKit
//
//  Created by Lachlan Grant on 19/12/16.
//  Copyright © 2016 Lachlan Grant. All rights reserved.
//

import Foundation

/// MKULog
public class MKULog {
    /// Singleton
    public static let shared = MKULog()
    
    init() {
        self.logDetails()
    }
    
    
    /// Debug Log
    ///
    /// - Parameter item: Log Data
    public func debug(_ item: Any) {
        if MKUAppSettings.shared.isDebugBuild {
            print("[DEBUG] \(item)")
            
            #if os(iOS)
                MKUConsole.print((item as AnyObject).description)
            #endif
        }
    }
    
    /// Error Log
    ///
    /// - Parameter item: Log Data
    public func error(_ item: Any) {
        print("[ERROR] \(item)")
        
        #if os(iOS)
            MKUConsole.error((item as AnyObject).description)
        #endif
    }
    
    /// Info Log
    ///
    /// - Parameter item: Log Data
    public func info(_ item: Any) {
        print("[INFO] \(item)")
        
        #if os(iOS)
            MKUConsole.print((item as AnyObject).description)
        #endif
    }
    
    
    /// Mark the Log with a seperator
    public func mark() {
        print("-------------------")
        
        #if os(iOS)
            MKUConsole.addMarker()
        #endif
    }
    
    /// Clear on screen log
    public func clear() {
        print(" -- Cleared on Screen Console -- ")
        
        #if os(iOS)
            MKUConsole.clear()
        #endif
    }
    
    /// Log App Name, Bundle ID, Version & Build
    public func logDetails() {
        let settings = MKUAppSettings.shared
        
        let str = "\(settings.displayName) (\(settings.bundleID)) version: \(settings.version) (\(settings.build))"
        self.info(str as Any)
        
        let str2 = "On \(settings.hostname)(\(settings.uptime))(\(settings.pID))"
        self.info(str2 as Any)
        self.mark()
    }
}

#if os(iOS)
    
    import UIKit
    import MessageUI
    
    public class MKUConsole {
        public static var shared = MKUConsole()
        var textView: UITextView?
        
        public static var textAppearance = [
            //TODO: Fix this line. NSFontAttributeName: UIFont(name: "Menlo", size: 12.0)!,
            NSAttributedStringKey.foregroundColor: UIColor.white]
        
        private init() {
        }
        
        var dateFormatter: DateFormatter = {
            let formatter = DateFormatter()
            formatter.dateStyle = DateFormatter.Style.none
            formatter.timeStyle = DateFormatter.Style.medium
            return formatter
        }()
        
        func currentTimeStamp() -> String {
            return dateFormatter.string(from: Date())
        }
        
        public static func scrollToBottom() {
            if let textView = shared.textView, textView.bounds.height < textView.contentSize.height {
                textView.layoutManager.ensureLayout(for: textView.textContainer)
                let offset = CGPoint(x: 0, y: (textView.contentSize.height - textView.frame.size.height))
                textView.setContentOffset(offset, animated: true)
            }
        }
        
        public static func print(_ text: String, color: UIColor = UIColor.white, global: Bool = true) {
            let formattedText = NSMutableAttributedString(string: text)
            let range = NSRange(location: 0, length: formattedText.length)
            
            // set standard text appearance and override foreground color attribute
            formattedText.addAttributes(MKUConsole.textAppearance, range: range)
            formattedText.addAttribute(NSAttributedStringKey.foregroundColor, value: color, range: range)
            
            MKUConsole.print(formattedText, global: global)
        }
        
        public static func print(_ text: NSAttributedString, global: Bool = true) {
            if let textView = shared.textView {
                DispatchQueue.main.async {
                    let timeStamped = NSMutableAttributedString(string: shared.currentTimeStamp() + " ")
                    let range = NSRange(location: 0, length: timeStamped.length)
                    
                    // set standard text appearance for time-stamp
                    timeStamped.addAttributes(MKUConsole.textAppearance, range: range)
                    
                    timeStamped.append(text)
                    timeStamped.append(NSAttributedString(string: "\n"))
                    
                    let newText = NSMutableAttributedString(attributedString: textView.attributedText)
                    newText.append(timeStamped)
                    
                    textView.attributedText = newText
                    MKUConsole.scrollToBottom()
                }
            }
            
            if global {
                //                Swift.print(text.string)
            }
        }
        
        public static func clear() {
            DispatchQueue.main.async {
                shared.textView?.text = ""
                MKUConsole.scrollToBottom()
            }
        }
        
        public static func error(_ text: String) {
            MKUConsole.print(text, color: UIColor.red)
        }
        
        public static func addMarker() {
            MKUConsole.print("-----------", color: UIColor.red)
        }
    }
    
    // deprecated functions
    
    extension MKUConsole {
        @available(*, deprecated, message: "use static function MKUConsole.scrollToBottom() instead")
        public func scrollToBottom() {
            MKUConsole.scrollToBottom()
        }
        
        @available(*, deprecated, message: "use static function MKUConsole.print(_:, global:) instead")
        public func print(text: String, global: Bool = true) {
            MKUConsole.print(text, global: global)
        }
        
        @available(*, deprecated, message: "use static function MKUConsole.clear() instead")
        public func clear() {
            MKUConsole.clear()
        }
        
        @available(*, deprecated, message: "use static function MKUConsole.addMarker() instead")
        public func addMarker() {
            MKUConsole.addMarker()
        }
    }
    
    public class MKUConsoleManager {
        public static let shared = MKUConsoleManager()
        
        public func getWindowWithRootViewController(_ root: UIViewController) -> UIWindow {
            let window = UIWindow()
            window.backgroundColor = .white
            
            let controller = MKUConsoleController(rootViewController: root)
            controller.consoleWindowMode = .expanded
            window.rootViewController = controller
            return window
        }
        
        public func getViewController() -> UIViewController {
            let controller = MKUConsoleViewController()
            return controller
        }
    }
    
    public class MKUConsoleController: UIViewController {
        
        /// the kind of window modes that are supported by MKUConsole
        ///
        /// - collapsed: the console is hidden
        /// - expanded: the console is shown
        
        public enum WindowMode {
            case collapsed
            case expanded
        }
        
        // MARK: - Private Properties -
        public var rootViewController: UIViewController
        
        public var consoleViewController: MKUConsoleViewController = {
            return MKUConsoleViewController()
        }()
        
        public lazy var consoleViewHeightConstraint: NSLayoutConstraint? = {
            return self.consoleViewController.view.heightAnchor.constraint(equalToConstant: 0)
        }()
        
        public let consoleFrameHeight: CGFloat = 120
        public let expandedHeight: CGFloat = 140
        
        public lazy var consoleFrame: CGRect = {
            
            var consoleFrame = self.view.bounds
            consoleFrame.size.height -= self.consoleFrameHeight
            
            return consoleFrame
        }()
        
        public var consoleWindowMode: WindowMode = .collapsed {
            didSet {
                consoleViewHeightConstraint?.isActive = false
                consoleViewHeightConstraint?.constant = consoleWindowMode == .collapsed ? 0 : self.expandedHeight
                consoleViewHeightConstraint?.isActive = true
            }
        }
        
        // MARK: - Initializer -
        public init(rootViewController: UIViewController) {
            self.rootViewController = rootViewController
            super.init(nibName: nil, bundle: nil)
        }
        
        required public init?(coder aDecoder: NSCoder) {
            assertionFailure("Interface Builder is not supported")
            self.rootViewController = UIViewController()
            super.init(coder: aDecoder)
        }
        
        // MARK: - Public Methods -
        override open func viewDidLoad() {
            super.viewDidLoad()
            
            addChildViewController(consoleViewController)
            consoleViewController.view.frame = consoleFrame
            view.addSubview(consoleViewController.view)
            consoleViewController.didMove(toParentViewController: self)
            
            addChildViewController(rootViewController)
            rootViewController.view.frame = CGRect(x: consoleFrame.minX,
                                                   y: consoleFrame.maxY,
                                                   width: view.bounds.width,
                                                   height: 120)
            view.addSubview(rootViewController.view)
            rootViewController.didMove(toParentViewController: self)
            
            setupConstraints()
        }
        
        open override func motionBegan(_ motion: UIEventSubtype, with event: UIEvent?) {
            if (motion == UIEventSubtype.motionShake) {
                consoleWindowMode = consoleWindowMode == .collapsed ? .expanded : .collapsed
                UIView.animate(withDuration: 0.25) {
                    self.view.layoutIfNeeded()
                }
            }
        }
        
        // MARK: - Private Methods -
        private func setupConstraints() {
            
            rootViewController.view.attach(anchor: .top, to: view)
            
            consoleViewController.view.attach(anchor: .bottom, to: view)
            consoleViewHeightConstraint?.isActive = true
            
            rootViewController.view.bottomAnchor.constraint(equalTo: consoleViewController.view.topAnchor).isActive = true
        }
    }
    
    @available(iOS 9.0, *)
    fileprivate extension UIView {
        
        enum Anchor {
            case top
            case bottom
        }
        
        func attach(anchor: Anchor, to view: UIView) {
            
            translatesAutoresizingMaskIntoConstraints = false
            
            switch anchor {
            case .top:
                topAnchor.constraint(equalTo: view.topAnchor).isActive = true
            case .bottom:
                bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
            }
            
            leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
            rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
            
        }
    }
    
    public class MKUConsoleViewController: UIViewController {
        let consoleTextView: UITextView = {
            let textView = UITextView()
            textView.backgroundColor = UIColor.black
            textView.isEditable = false
            return textView
        }()
        
        override public func viewDidLoad() {
            super.viewDidLoad()
            MKUConsole.shared.textView = consoleTextView
            view.addSubview(consoleTextView)
            
            //        let addMarkerGesture = UISwipeGestureRecognizer(target: self, action: #selector(addMarker))
            //        view.addGestureRecognizer(addMarkerGesture)
            
            //        let addCustomTextGesture = UITapGestureRecognizer(target: self, action: #selector(customText))
            //        addCustomTextGesture.numberOfTouchesRequired = 2
            //        view.addGestureRecognizer(addCustomTextGesture)
            
            //        let showAdditionalActionsGesture = UITapGestureRecognizer(target: self, action: #selector(additionalActions))
            //        showAdditionalActionsGesture.numberOfTouchesRequired = 3
            //        view.addGestureRecognizer(showAdditionalActionsGesture)
            
            setupConstraints()
            
            MKULog.shared.logDetails()
        }
        
        func setupConstraints() {
            consoleTextView.translatesAutoresizingMaskIntoConstraints = false
            consoleTextView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
            consoleTextView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
            consoleTextView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
            consoleTextView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        }
        
        //    func customText(sender: UITapGestureRecognizer) {
        //        let alert = UIAlertController(title: "Custom Log",
        //                                      message: "Enter text you want to log.",
        //                                      preferredStyle: UIAlertControllerStyle.alert)
        //        alert.addTextField { (textField: UITextField) in
        //            textField.keyboardType = .alphabet
        //        }
        //
        //        let okAction = UIAlertAction(title: "Add log", style: UIAlertActionStyle.default) {
        //            (action: UIAlertAction) in
        //            if let text = alert.textFields?.first?.text, !text.isEmpty {
        //                MKUConsole.print(text)
        //            }
        //        }
        //
        //        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil)
        //
        //        alert.addAction(okAction)
        //        alert.addAction(cancelAction)
        //
        //        present(alert, animated: true, completion: nil)
        //    }
        //
        //    func additionalActions(sender: UITapGestureRecognizer) {
        //        let alert = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        //
        //        let sendMail = UIAlertAction(title: "Send Email", style: UIAlertActionStyle.default) {
        //            (action: UIAlertAction) in
        //            DispatchQueue.main.async {
        //                if let text = MKUConsole.shared.textView?.text {
        //                    let composeViewController = MFMailComposeViewController()
        //                    composeViewController.mailComposeDelegate = self
        //                    composeViewController.setSubject("Console Log")
        //                    composeViewController.setMessageBody(text, isHTML: false)
        //                    self.present(composeViewController, animated: true, completion: nil)
        //                }
        //            }
        //        }
        //
        //        let clearAction = UIAlertAction(title: "Clear", style: UIAlertActionStyle.destructive) {
        //            (action: UIAlertAction) in
        //            MKUConsole.clear()
        //        }
        //
        //        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil)
        //
        //        alert.addAction(sendMail)
        //        alert.addAction(clearAction)
        //        alert.addAction(cancelAction)
        //
        //        present(alert, animated: true, completion: nil)
        //    }
        //
        //    func addMarker(sender: UISwipeGestureRecognizer) {
        //        MKUConsole.addMarker()
        //    }
    }
    
    extension MKUConsoleViewController: MFMailComposeViewControllerDelegate {
        public func mailComposeController(_ controller: MFMailComposeViewController,
                                          didFinishWith result: MFMailComposeResult,
                                          error: Error?) {
            controller.dismiss(animated: true, completion: nil)
        }
    }
    
#endif
