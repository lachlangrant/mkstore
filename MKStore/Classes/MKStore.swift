//
//  MKStore.swift
//  MKKit
//
//  Created by Lachlan Grant on 19/12/16.
//  Copyright © 2016 Lachlan Grant. All rights reserved.
//

import Foundation
import MKUtility
import SwiftyStoreKit

public class MKStore {

	public var canMakePayments: Bool {
		return SwiftyStoreKit.canMakePayments
	}

	public init() {
	}

	public func setListener(_ returnVoid: @escaping ([Purchase]) -> ()) {
		SwiftyStoreKit.completeTransactions(completion: returnVoid)
	}

	public func getProductInformation(productId: String, completion: @escaping (_ success: Bool, _ title: String?, _ description: String?, _ price: String?) -> Void) {

		SwiftyStoreKit.retrieveProductsInfo([productId]) { (result) in
			if let product = result.retrievedProducts.first {
				let numberFormatter = NumberFormatter()
				numberFormatter.locale = product.priceLocale
				numberFormatter.numberStyle = .currency

				completion(true, product.localizedTitle, product.localizedDescription, numberFormatter.string(from: product.price))
			}
		}

		completion(false, nil, nil, nil)

	}

	public func buyProductWith(id: String, completion: @escaping (_ success: Bool, _ productId: String?, _ error: String?) -> Void) {
        SwiftyStoreKit.purchaseProduct(id) { (result) in
            switch result {
            case .success(purchase: let details):
                MKULog.shared.info("Purchase Success: \(details.product)")
                completion(true, details.product.productIdentifier, nil)
                
            case .error(error: let _):
                let errString = "Unable to Purchase"
                MKULog.shared.error("[MKStore] [buyProduct] \(id) \(errString)")
                completion(false, nil, errString)
            }
        }
	}

	public func restorePurcheses(completion: @escaping (_ success: Bool, _ productIds: [Purchase]?) -> Void) {
		SwiftyStoreKit.restorePurchases { (results) in
			if results.restoreFailedPurchases.count > 0 {
				MKULog.shared.error("Restore Failed: \(results.restoreFailedPurchases)" as AnyObject)
				completion(false, nil)
			} else if results.restoredPurchases.count > 0 {
				MKULog.shared.info("Restore Success: \(results.restoredPurchases)" as AnyObject)
				completion(true, results.restoredPurchases)
			} else {
				completion(false, [])
			}
		}
	}
}
