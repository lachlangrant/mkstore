# MKStore

[![CI Status](http://img.shields.io/travis/lachlangrant/MKStore.svg?style=flat)](https://travis-ci.org/lachlangrant/MKStore)
[![Version](https://img.shields.io/cocoapods/v/MKStore.svg?style=flat)](http://cocoapods.org/pods/MKStore)
[![License](https://img.shields.io/cocoapods/l/MKStore.svg?style=flat)](http://cocoapods.org/pods/MKStore)
[![Platform](https://img.shields.io/cocoapods/p/MKStore.svg?style=flat)](http://cocoapods.org/pods/MKStore)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

MKStore is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'MKStore'
```

## Author

lachlangrant, lachlangrant@rbvea.co

## License

MKStore is available under the MIT license. See the LICENSE file for more info.
